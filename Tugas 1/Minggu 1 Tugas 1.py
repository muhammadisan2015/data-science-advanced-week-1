#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 25 10:53:11 2020

@author: bokab
"""

"""
1. Pengalaman saya menggunakan python adalah mengolah data Open cluster bintang 
   pada tugas akhir masa kuliah.
2. Untuk kelas Data Science, mungkin saya berharap ada sejenis ML pada fotografi.
   Karena pertanyaannya adalah kelas advanced python (bukan Data Science), saya 
   berharap ada kelas web framework menggunakan Flask atau Djanggo

"""

class manipulasi:
    kalimat = ""
    
    def __init__(self, data):
        self.kalimat = data

    def upper_word(self):
        return self.kalimat.upper()
    
    def lower_word(self):
        return self.kalimat.lower()
    
    def capt_word(self):
        return self.kalimat.capitalize()
    
    def sep_word(self):
        return list(self.kalimat.split(" "))


def start():
    data = "Saya tingGAl di indOnesia"
    f = manipulasi(data)
    print(f.upper_word())
    print(f.lower_word())
    print(f.capt_word())
    print(f.sep_word())

if __name__ == "__main__":
    start()